# Introduction to Python
This repository contains Jupyter notebooks that aim to introduce Python 3.0 as
quickly as possible. They also introduce standard Python libraries like
matplotlib and numpy.

## Prerequisites
The notebooks assume a basic familiarity with programming concepts like
conditions and loops.

## License
This repository is released under the Creative Commons Attribution 4.0
International License. To know more about the terms and conditions, please visit
this [link](https://creativecommons.org/licenses/by/4.0/)
